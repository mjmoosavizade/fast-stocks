from fastapi import FastAPI, Request
from fastapi.templating import Jinja2Templates

app = FastAPI()

templates = Jinja2Templates(directory="templates")

@app.get("/")
def home(request: Request):
    context = {
        "request": request
    }
    return templates.TemplateResponse("home.html",context)

@app.post("/stock")
def create_stock():
    """
        create a  stock in database
    """
    return {
        "code":"success",
        "message":"stock created"
        }